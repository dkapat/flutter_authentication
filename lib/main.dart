import 'package:flutter/material.dart';
import './services/auth_service.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Login Demo',
      theme: ThemeData(
          primarySwatch: Colors.indigo,
        ),
        home: AuthService().handleAuth(),
    );
  }
}
