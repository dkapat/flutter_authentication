import 'package:flutter/material.dart';
import '../services/auth_service.dart';

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: RaisedButton(
          onPressed: (){
            AuthService().signOut();
          },
          child: Text('Signout'),
        ),
      ),
    );
  }
}
